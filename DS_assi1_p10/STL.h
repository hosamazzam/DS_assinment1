#include <iostream>
#include <set>
using namespace std;

template <class type>
class STL
{
private:

    set<type>Set;
    typename set<type>::iterator it;
public:
    bool operator==( STL<type> &set2);
    bool operator!=( STL<type> &set2);
    STL operator+ ( STL<type> &set2);
    STL operator* ( STL<type> &set2);
    STL operator- ( STL<type> &set2);
    void operator= ( STL<type>  set2);
    friend istream &operator>>(istream &input, STL<type> &_stl)
    {
        type word;
        input>>word;
        _stl.Set.insert(word);

        return input;
    }
    friend ostream &operator<<(ostream &out, STL<type> &_stl)
    {

        for(_stl.it=_stl.Set.begin(); _stl.it !=_stl.Set.end(); ++_stl.it)
        {
            out<<*_stl.it<<" ";
        }
        out<<endl;

        return out;
    }
};

template <class type>
bool STL<type>::operator==( STL<type> &_stl)
{
    if(this->Set.size()!=_stl.Set.size())
        return false;
    else
    {
        this->it=this->Set.begin();
        for(_stl.it=_stl.Set.begin(); _stl.it !=_stl.Set.end(); ++_stl.it)
        {
            if(*_stl.it !=*this->it)
                return false;
            ++this->it;
        }
    }
    return true;
}

template <class type>
bool STL<type>::operator!=( STL<type> &_stl)
{
    if(this->Set.size()!=_stl.Set.size())
        return true;
    else
    {
        this->it=this->Set.begin();
        for(_stl.it=_stl.Set.begin(); _stl.it !=_stl.Set.end(); ++_stl.it)
        {
            if(*_stl.it !=*this->it)
                return true;
            ++this->it;
        }
    }
    return false;
}

template <class type>
void STL<type>::operator=( STL<type> _stl)
{
    this->Set.clear();
    for(_stl.it=_stl.Set.begin(); _stl.it !=_stl.Set.end(); ++_stl.it)
    {
        this->Set.insert(*_stl.it);
    }
}

template <class type>
STL<type> STL<type>::operator+( STL<type> &_stl)
{
    STL<int> stl;
    for(_stl.it=_stl.Set.begin(); _stl.it !=_stl.Set.end(); ++_stl.it)
    {
        stl.Set.insert(*_stl.it);
    }
    for(this->it=this->Set.begin(); this->it !=this->Set.end(); ++this->it)
    {
        stl.Set.insert(*this->it);
    }

    return stl;
}

template <class type>
STL<type> STL<type>::operator*( STL<type> &_stl)
{
    STL<int> stl;
    for(_stl.it=_stl.Set.begin(); _stl.it !=_stl.Set.end(); ++_stl.it)
    {
        for(this->it=this->Set.begin(); this->it !=this->Set.end(); ++this->it)
        {
            if(*_stl.it == *this->it)
            {
                stl.Set.insert(*this->it);
                break;
            }

        }
    }
    return stl;
}

template <class type>
STL<type> STL<type>::operator-( STL<type> &_stl)
{
    STL<int> stl;
    for(_stl.it=_stl.Set.begin(); _stl.it !=_stl.Set.end(); ++_stl.it)
    {
        bool flag= true;
        for(this->it=this->Set.begin(); this->it !=this->Set.end(); ++this->it)
        {
            if(*_stl.it == *this->it)
            {
                flag = false;
                break;
            }
        }
        if(flag)
        {
            stl.Set.insert(*_stl.it);
        }
    }
    return stl;
}
