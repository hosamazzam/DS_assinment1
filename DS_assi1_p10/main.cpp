#include <iostream>
#include "STL.h"
using namespace std;

int main()
{
    STL<int> s1,s2,s3;
    int counter;
    cout<<"Set 1 size : ";
    cin>>counter;
    cout<<"Enter set 1 element : "<<endl;
    for(int i= 0; i<counter; i++)
    {
        cin>>s1;
    }
    cout<<"Set 2 size : ";
    cin>>counter;
    cout<<"Enter set 2 element : "<<endl;
    for(int i= 0; i<counter; i++)
    {
        cin>>s2;
    }
    cout<<endl;
    cout<<"s1 : "<<s1<<endl;
    cout<<"s2 : "<<s2<<endl;
    cout<<"s3 : "<<s3<<endl;
    if(s1==s2)
    {
        cout<<"s1,s2 are equal"<<endl;
    }
    if(s1!=s2)
    {
        cout<<"s1,s2 are not equal"<<endl;
    }
    s3=s1+s2;
    cout<<"\ns3=s1+s2 : "<<s3<<endl;
    s3=s1*s2;
    cout<<"s3=s1*s2 : "<<s3<<endl;
    s3=s1-s2;
    cout<<"s3=s1-s2 : "<<s3<<endl;


    return 0;
}
