#include "Matrix.h"
#include <iostream>
using namespace std;

template <class type>
Matrix<type>::Matrix()
{
    rows=1;
    cols=1;
    mat =NULL;
}

template <class type>
Matrix<type>::Matrix(int _rows,int _cols)
{
    rows=_rows;
    cols=_cols;
    mat= NULL;
    mat = new int*[rows];
    for(int i = 0; i < rows; ++i)
        mat[i] = new type[cols];
}

template <class type>
Matrix<type>::~Matrix()
{
    rows=0;
    cols=0;
    for (int i=0; i<rows; i++)
        delete mat[i];
    delete [] mat;
}

template <class type>
int Matrix<type>::getRows()
{
    return rows;
}
template <class type>
int Matrix<type>::getCols()
{
    return cols;
}

template <class type>
Matrix<type> Matrix<type>::operator+ (Matrix<type>& _matrix)
{
    Matrix<type> matrix(_matrix.getRows(),_matrix.getCols());

    for (int i=0; i<_matrix.rows; i++)
    {
        for (int j=0; j<_matrix.cols; j++)
        {

            matrix.mat[i][j]=this->mat[i][j]+ _matrix.mat[i][j];
        }
    }
    return matrix;

}

template <class type>
Matrix<type> Matrix<type>::operator- (Matrix<type>& _matrix)
{
    Matrix<type> matrix(_matrix.getRows(),_matrix.getCols());

    for (int i=0; i<_matrix.rows; i++)
    {
        for (int j=0; j<_matrix.cols; j++)
            matrix.mat[i][j]=this->mat[i][j]- _matrix.mat[i][j];
    }
    return matrix;
}

template <class type>
Matrix<type> Matrix<type>::operator* (Matrix<type>& _matrix)
{
    Matrix<type> matrix(this->getRows(),_matrix.getCols());

    for (int i=0; i<matrix.rows; i++)
    {
        for (int j=0; j<matrix.cols; j++)
        {
            type sum=0;
            for(int z=0; z<_matrix.getRows(); z++)
            {
                sum+=(this->mat[i][z]*_matrix.mat[z][j]);
            }
            matrix.mat[i][j]= sum;
        }
    }
    return matrix;
}

template <class type>
void Matrix<type>::operator= (Matrix<type> _matrix)
{
    this->cols = _matrix.cols;
    this->rows = _matrix.rows;
    for (int i=0; i<this->rows; i++)
    {
        for (int j=0; j<this->cols; j++)
        {

            this->mat[i][j]= _matrix.mat[i][j];

        }

    }
}

template <class type>
Matrix<type> Matrix<type>::transpose(Matrix<type> _matrix)
{
    Matrix<type> matrix(_matrix.getCols(),_matrix.getRows());
    for(int i = 0; i<_matrix.rows; i++)
    {
        for(int j=0; j<_matrix.cols; j++)
        {
            matrix.mat[j][i]= _matrix.mat[i][j];
        }
    }
    return matrix;
}

