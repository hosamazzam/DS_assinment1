#include <iostream>
#include <stdlib.h>
#include "Matrix.cpp"
using namespace std;

int main()
{
    int r,c;

    cout<<"Please enter dimensions of matrix 1 : ";
    cin>>r>>c;
    cout<<"Enter matrix elements :"<<endl;
    Matrix<int> m1(r,c);
    cin>>m1;
    cout<<"Please enter dimensions of matrix 2 : ";
    cin>>r>>c;
    cout<<"Enter matrix elements :"<<endl;
    Matrix<int> m2(r,c);
    cin>>m2;
    system("CLS");
    int chos =0;
    while(chos!=6)
    {
        system("cls");
        cout<<"Welcome to (Hos) Matrix Calculator"<<endl;
        cout<<"----------------------------------------"<<endl;
        cout<<"1- Perform Matrix Addition"<<endl;
        cout<<"2- Perform Matrix Subtraction"<<endl;
        cout<<"3- Perform Matrix Multiplication"<<endl;
        cout<<"4- Matrix Transpose"<<endl;
        cout<<"5- Exit"<<endl;
        cout<<"You choice : ";
        cin>>chos;
        if(chos==1)
        {
            system("cls");
            if(m1.getRows()==m2.getRows()&& m1.getCols()==m2.getCols())
            {
                Matrix<int> m3(m1.getRows(),m1.getCols());
                m3=m1+m2;
                cout<<m3<<endl;
                system("pause");
            }
            else
            {
                cout<<"can't perform this operation"<<endl;
                system("pause");
            }
        }
        else if (chos==2)
        {
            system("cls");
            if(m1.getRows()==m2.getRows()&& m1.getCols()==m2.getCols())
            {
                Matrix<int> m3(m1.getRows(),m1.getCols());
                m3=m1-m2;
                cout<<m3<<endl;
                system("pause");
            }
            else
            {
                cout<<"can't perform this operation"<<endl;
                system("pause");
            }
        }
        else if (chos==3)
        {
            system("cls");
            if(m1.getCols()==m2.getRows())
            {
                Matrix<int> m3(m1.getRows(),m2.getCols());
                m3=m1*m2;
                cout<<m3<<endl;
                system("pause");
            }
            else
            {
                cout<<"can't perform this operation"<<endl;
                system("pause");
            }
        }

        else if (chos==4)
        {
            system("cls");
            cout<<"1- First Matrix\n2- Second Matrix\nYour choice : ";
            int select=0;
            cin>>select;
            if(select==1)
            {
                Matrix<int> m3(m1.getCols(),m1.getRows());
                m3 =m1.transpose(m1);
                cout<<m3<<endl;
                system("pause");
            }
            else if (select==2)
            {
                Matrix<int> m3(m2.getCols(),m2.getRows());
                m3=m2.transpose(m2);
                cout<<m3<<endl;
                system("pause");
            }
            else
            {
                cout<<"can't perform this operation"<<endl;
                system("pause");
            }

        }

        else if (chos==5)
        {
            system("cls");
            cout<<"Thanks you :)"<<endl;
            system("pause");
            chos=6;
        }

    }
    return 0;
}
