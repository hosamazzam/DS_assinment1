#include <iostream>
using namespace std;

template <class type>
class Matrix
{
private:
    int rows;
    int cols;
    type **mat;

public:
    Matrix();
    Matrix(int _rows,int _cols);
    Matrix operator+(Matrix<type>& _matrix);
    Matrix operator- (Matrix<type>& _matrix);
    void operator= (Matrix<type> _matrix);
    Matrix operator* (Matrix<type>& _matrix);
    Matrix transpose(Matrix<type> _matrix);
    ~Matrix();
    int getRows();
    int getCols();

    friend istream &operator>>(istream &input, Matrix<type> &_matrix)
    {
        for (int i=0; i<_matrix.rows; i++)
        {
            for (int j=0; j<_matrix.cols; j++)
                input>>_matrix.mat[i][j];
        }
        return input;
    }

    friend ostream &operator<<(ostream &out, Matrix<type> &_matrix)
    {
        for (int i=0; i<_matrix.rows; i++)
        {
            for (int j=0; j<_matrix.cols; j++)
            {
                out<<_matrix.mat[i][j]<<"  " ;
            }
            out<<endl;
        }
        return out;
    }




};

